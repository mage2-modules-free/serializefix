#Magento 2.2.x Serialize Fix

## Requirements:
- Magento 2.2.x
- PHP7+

## Instructions for using composer

Use composer to install this extension. First make sure that Magento is installed via composer, and that there is a valid `composer.json` file present.

Next, Connect to the The Dutchmen Composer Repository:

    composer config repositories.thedutchmen_serializefix git git@gitlab.com:mage2-modules-free/serializefix.git

Next, install our module using the following command:

    composer require thedutchmen/module-serializefix:dev-master
    
Next, install the new module into Magento itself:

    php bin/magento module:enable TheDutchmen_SerializeFix
    php bin/magento setup:upgrade

Done

## Instructions for manual copy

We recommend `composer` to install this package. If you want a manual copy instead, these are the steps. However, please note that we do NOT recommend you to do this.

- Upload the files in the source/ folder to the folder app/code/TheDutchmen/SerializeFix of your site
- Run php -f bin/magento module:enable TheDutchmen_SerializeFix
- Run php -f bin/magento setup:upgrade
- Flush the Magento cache
- Done

## Extension Update via Composer

The extension update process is the same as the module installation process. To update the extension, simply execute the command:

    composer update thedutchmen/module-serializefix

To update the version from 1.x.x to 2.x.x you need to execute the following command:

    composer require thedutchmen/module-serializefix ^x.0.0 --update-with-dependencies 
    
Where x corresponds to the first version number. For example, to update the Google Tag Manager Extension for M2 from the 1.x.x version to the 2.x.x version you need to execute the command:

    composer require thedutchmen/module-serializefix ^2.0.0 --update-with-dependencies
     
To update all modules execute the command:

    composer update 
    
## Delete Extension via Composer

To delete the extension via Composer, please execute the command:

    composer remove thedutchmen/module-serializefix


